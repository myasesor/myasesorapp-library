package com.myasesorapplibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyasesorappLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyasesorappLibraryApplication.class, args);
	}

}
